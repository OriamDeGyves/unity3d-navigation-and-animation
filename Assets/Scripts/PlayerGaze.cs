﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGaze : MonoBehaviour
{
    public Transform target;
    [Range(0.0f, 1.0f)]
    public float weight;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        _animator.SetLookAtPosition(target.position);
        _animator.SetLookAtWeight(weight, weight, weight, weight);
    }
}
