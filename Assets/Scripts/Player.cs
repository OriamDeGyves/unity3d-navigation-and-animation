﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public Camera cam;
    public Animator animator;
    public PlayerGaze gaze;

    private NavMeshAgent _agent;
    private Coroutine _jumpCoroutine = null;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000.0f, 1, QueryTriggerInteraction.Ignore))
            {
                _agent.SetDestination(hit.point);
            }
        }
        animator.SetFloat("Speed", _agent.velocity.magnitude);

        if (_agent.isOnOffMeshLink && _jumpCoroutine == null)
        {
            _jumpCoroutine = StartCoroutine(JumpLink());
        }

    }

    private IEnumerator JumpLink()
    {
        OffMeshLinkData data = _agent.currentOffMeshLinkData;
        Vector3 endPosition = data.endPos + Vector3.up * _agent.baseOffset;
        animator.SetTrigger("Jump");
        while (_agent.transform.position != endPosition)
        {
            _agent.transform.position = Vector3.MoveTowards
                (_agent.transform.position, endPosition,
                _agent.speed * 1.6f * Time.deltaTime);

            yield return null;
        }

        _agent.CompleteOffMeshLink();
        _jumpCoroutine = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Interesting"))
        {
            gaze.weight = 1.0f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Interesting"))
        {
            gaze.weight = 0.0f;
        }
    }
}
