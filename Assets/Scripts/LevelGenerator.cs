﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelGenerator : MonoBehaviour
{
    public int width = 10;
    public int height = 10;

    public GameObject wallPrefab;
    public GameObject playerPrefab;

    public new Camera camera;
    public NavMeshSurface navMesh;

    private bool _playerSpawned = false;

    void Start()
    {
        GenerateLevel();

        navMesh.BuildNavMesh();
    }

    // Create a grid based level
    void GenerateLevel()
    {
        // Loop over the grid
        for (int x = 0; x <= width; x += 2)
        {
            for (int y = 0; y <= height; y += 2)
            {
                // Should we place a wall?
                if (Random.value > .7f)
                {
                    // Spawn a wall
                    Vector3 pos = new Vector3(x - width / 2f, 1f, y - height / 2f);
                    Instantiate(wallPrefab, pos, Quaternion.identity, transform);
                }
                else if (!_playerSpawned) // Should we spawn a player?
                {
                    // Spawn the player
                    Vector3 pos = new Vector3(x - width / 2f, 0.0f, y - height / 2f);
                    GameObject playerGO = Instantiate(playerPrefab, pos, Quaternion.identity);
                    Player playerScript = playerGO.GetComponent<Player>();
                    playerScript.cam = camera;
                    _playerSpawned = true;
                }
            }
        }
    }
}